import { Injectable } from '@angular/core';
import { Item } from './items/item';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ItemsService {
  private url: string = 'https://localhost:7288/api/Items';

  constructor(private http: HttpClient) {}

  httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'my-auth-token',
    }),
  };

  //Read all data
  getItems(): Observable<Item[]> {
    let response = this.http.get<Item[]>(this.url);
    response.subscribe((Response) => console.log(Response));
    return this.http.get<Item[]>(this.url);
  }
  //Create new item
  createItem(variable: any): void {
    variable.id = 0;
    let requestObject = JSON.stringify(variable);
    this.http.post(this.url, requestObject, this.httpOptions).subscribe();
  }
  //Read a single item
  getItem(id?: number): Observable<Item> {
    return this.http.get<Item>(this.url + '/' + id);
  }
  //Update existing item
  updateItem(variable: any): void {
    let requestObject = JSON.stringify(variable);
    this.http
      .put(this.url + '/' + variable.id, requestObject, this.httpOptions)
      .subscribe((Response) => console.log(Response));
  }
  //Delete existing item
  deleteItem(id?: number): void {
    this.http
      .delete(this.url + '/' + id)
      .subscribe((Response) => console.log(Response));
  }
}
