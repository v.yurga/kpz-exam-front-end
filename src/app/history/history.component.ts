import { Component, OnInit } from '@angular/core';
import { HistoryService } from '../history.service';
import { Log } from './log';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
})
export class HistoryComponent implements OnInit {
  history?: Log[];
  selectedLog: Log = {
    id: 0,
    surname: '',
    itemId: 0,
    exploitationYearBegin: 0,
    exploitationYearEnd: 0,
  };
  constructor(private historyService: HistoryService) {}

  ngOnInit(): void {
    this.historyService
      .getHistory()
      .subscribe((Response) => (this.history = Response));
  }

  onCreate(): void {
    this.historyService.createLog(this.selectedLog);
    window.location.reload();
  }
  onRead(): void {
    this.historyService
      .getLog(this.selectedLog.id)
      .subscribe((Response) => (this.selectedLog = Response));
  }

  onUpdate(): void {
    this.historyService.updateLog(this.selectedLog);
    window.location.reload();
  }
  onDelete(): void {
    this.historyService.deleteLog(this.selectedLog?.id);
    window.location.reload();
  }
}
