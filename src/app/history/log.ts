export interface Log {
  id: number;
  surname: string;
  itemId: number;
  exploitationYearBegin: number;
  exploitationYearEnd: number;
}
