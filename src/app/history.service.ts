import { Injectable } from '@angular/core';
import {} from './items/item';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Log } from './history/log';

@Injectable({
  providedIn: 'root',
})
export class HistoryService {
  private url: string = 'https://localhost:7288/api/History';

  constructor(private http: HttpClient) {}

  httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'my-auth-token',
    }),
  };
  //Read all data
  getHistory(): Observable<Log[]> {
    let response = this.http.get<Log[]>(this.url);
    response.subscribe((Response) => console.log(Response));
    return this.http.get<Log[]>(this.url);
  }
  //Create new log
  createLog(variable: any): void {
    variable.id = 0;
    let requestObject = JSON.stringify(variable);
    this.http.post(this.url, requestObject, this.httpOptions).subscribe();
  }
  //Read a single log
  getLog(id?: number): Observable<Log> {
    return this.http.get<Log>(this.url + '/' + id);
  }
  //Update existing log
  updateLog(variable: any): void {
    let requestObject = JSON.stringify(variable);
    this.http
      .put(this.url + '/' + variable.id, requestObject, this.httpOptions)
      .subscribe((Response) => console.log(Response));
  }
  //Delete existing log
  deleteLog(id?: number): void {
    this.http
      .delete(this.url + '/' + id)
      .subscribe((Response) => console.log(Response));
  }
}
