import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../items.service';
import { Item } from './item';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css'],
})
export class ItemsComponent implements OnInit {
  items?: Item[] = [
    {
      id: 1,
      name: 'Vova',
      yearProduction: 2010,
      writeOffYear: 2022,
      price: 2000,
      room: 3,
    },
    {
      id: 1,
      name: 'Ihor',
      yearProduction: 2010,
      writeOffYear: 2022,
      price: 2000,
      room: 3,
    },
  ];
  selectedItem: Item = {
    id: 0,
    name: '',
    yearProduction: 0,
    writeOffYear: 0,
    price: 0,
    room: 0,
  };
  constructor(private itemsService: ItemsService) {}

  ngOnInit(): void {
    this.itemsService
      .getItems()
      .subscribe((Response) => (this.items = Response));
  }

  onCreate(): void {
    this.itemsService.createItem(this.selectedItem);
    window.location.reload();
  }
  onRead(): void {
    this.itemsService
      .getItem(this.selectedItem.id)
      .subscribe((Response) => (this.selectedItem = Response));
  }

  onUpdate(): void {
    this.itemsService.updateItem(this.selectedItem);
    window.location.reload();
  }
  onDelete(): void {
    this.itemsService.deleteItem(this.selectedItem?.id);
    window.location.reload();
  }
}
