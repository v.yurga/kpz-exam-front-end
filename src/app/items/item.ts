export interface Item {
  id: number;
  name: string;
  price: number;
  yearProduction: number;
  room: number;
  writeOffYear: number;
}
